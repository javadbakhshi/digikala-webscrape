# digikala-webscraver

set terminal to cmd

Create virtual environment: virtualenv env

myenv\scripts\activate

pip install -r requirements.txt

python manage.py runserver

http://localhost:8000/


I used bs 4 because of its simplicity and efficiency, and in the future, other libraries such as cloudscraper, selenium and scarpy can be used to improve the program, although this library does not have a good middle ground with javascript codes and can be used by getting api From the reference site it did it without using these libraries, I also used restframework with rest listapiview to display the api.
